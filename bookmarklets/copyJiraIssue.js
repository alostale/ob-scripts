// Bookmarktles to be included in the browser
//
// Copies Jira issue Key and title
javascript:(()=>{
    if (document.URL.indexOf('openbravo.atlassian.net')==-1){
      alert('To be executed in Openbravo Jira'); return;
    }
    const url = document.URL.replace(/\?filter=.*/,'');
    const html = document.title.replace(/^\[(RM-\d*)\]/,`<a href="${url}">$1</a>:`).replace(/ - Jira$/,'');
    const clipboardItem = new ClipboardItem({'text/html': new Blob([html], { type: 'text/html' })});
    navigator.clipboard.write([clipboardItem]);
})()

// Copies Jira issue Key
javascript:(()=>{
    if (document.URL.indexOf('openbravo.atlassian.net')==-1){
      alert('To be executed in Openbravo Jira'); return;
    }
    const url = document.URL.replace(/\?filter=.*/,'');
    const html = document.title.replace(/^\[(RM-\d*)\].*/,`<a href="${url}">$1</a>`);
    const clipboardItem = new ClipboardItem({'text/html': new Blob([html], { type: 'text/html' })});
    navigator.clipboard.write([clipboardItem]);
})()

