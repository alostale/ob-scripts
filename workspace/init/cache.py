#!/usr/bin/env python3
import gitlab
import json
import collections
from xml.dom.minidom import parseString


def getValue(module, tag):
    return module.getElementsByTagName(tag)[0].firstChild.data


def cacheModulesInfo(mods):
    cache = {}
    allMods = mods.projects.list(all=True)
    i = 0
    for p in allMods:
        i += 1
        print(f'  {i}/{len(allMods)} {p.name}')
        # Fetching a list of projects, doesn’t include all attributes of all projects. To retrieve all attributes, you’ll need to fetch a single project
        pr = gl.projects.get(p.id)

        try:
            f = pr.files.get(
                file_path='src-db/database/sourcedata/AD_MODULE.xml', ref='master')
        except:
            print(f'    Not AD_MODULE.xml found!')
            continue

        m = parseString(f.decode())

        module = {}
        module['javapackage'] = getValue(m, 'JAVAPACKAGE')
        module['path'] = p.ssh_url_to_repo
        module['name'] = getValue(m, 'NAME')

        deps = []
        try:
            dep = pr.files.get(
                file_path='src-db/database/sourcedata/AD_MODULE_DEPENDENCY.xml', ref='master')
            if dep:
                ds = parseString(dep.decode())
                for d in ds.getElementsByTagName('AD_MODULE_DEPENDENCY'):
                    dependency = {}
                    dependency['id'] = getValue(d, 'AD_DEPENDENT_MODULE_ID')
                    dependency['name'] = getValue(d, 'DEPENDANT_MODULE_NAME')
                    deps.append(dependency)
        except:
            print('     No dependencies found!')

        module['deps'] = deps
        cache[getValue(m, 'AD_MODULE_ID')] = module
    return cache


gl = gitlab.Gitlab.from_config('default', ['gl.cfg'])

modsCache = {}

print('## pmods ##')
pmods = gl.groups.get('6552354')
modsCache.update(cacheModulesInfo(pmods))

print('## mods ##')
mods = gl.groups.get('6552340')
modsCache.update(cacheModulesInfo(mods))


with open("modules-cache.json", "w") as cacheFile:
    json.dump(collections.OrderedDict(
        sorted(modsCache.items())), cacheFile, indent=2)
