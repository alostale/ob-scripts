#!/usr/bin/env python3
import json
import argparse
import re
import os
import subprocess

ob3 = ['0',
       'F8D1B3ECB3474E8DA5C216473C840DF1',
       'FF8080813129ADA401312CA1222A0005',
       'FF8080812D842086012D844F3CC0003E',
       '5EB4F15C80684ACA904756BDC12ADBE5',
       '0A060B2AF1974E8EAA8DB61388E9AECC',
       'C70732EA90A14EC0916078B85CC33D2D',
       '2758CD25B2704AF6BBAD10365FC82C06',
       '8A098711BB324335A19833286BDB093D',
       '9BA0836A3CD74EE4AB48753A47211BCC',
       '3A3A943684D64DEF9EC39F588A656848',
       'D393BE6F22BB44B7B728259B34FC795A',
       '96998CBC42744B3DBEE28AC8095C9335',
       '2A5EE903D7974AC298C0504FBC4501A7',
       'A918E3331C404B889D69AA9BFAFB23AC',
       '8A34B301DC524EA3A07513DF9F42CC90',
       'EC356CEE3D46416CA1EBEEB9AB82EDB9',
       '7E48CDD73B7E493A8BED4F7253E7C989',
       '883B5872CA0548F9AF2BBBE7D2DDFA61',
       '0138E7A89B5E4DC3932462252801FFBC',
       '4B828F4D03264080AA1D2057B13F613C',
       'FF8080813141B198013141B86DD70003',
       'A44B9BA75C354D8FB2E3F7D6EB6BFDC4']

messages = {"error": [], "warn": [], "info": []}

trl = re.compile(r'.*\.\w\w_\w\w')


with open(os.path.dirname(os.path.realpath(__file__)) + "/modules-cache.json", "r") as fp:
    cache = json.load(fp)


def printDependencies(modId, deps, level=0):
    tab = '  ' * level
    mod = cache.get(modId)
    deps.append(modId)
    if not mod:
        print('Not found module', modId)
        return

    print(f"{tab}{modToString(modId)}")

    errors = messages.get('error')
    for d in mod.get('deps'):
        depId = d.get('id')
        if depId in deps and not args.verbose:
            continue

        dep = cache.get(depId)
        if dep:
            printDependencies(d.get('id'), deps, level + 1)
        elif depId not in ob3:
            msg = f"Not found dependency {depId} - {d.get('name')}"
            errors.append(msg)
            print(tab, msg)


def modToString(modId):
    mod = cache.get(modId)
    return f"{mod.get('javapackage')} - {mod.get('name')} [{modId}]"


def matchToString(m):
    return f" {'*' if m.get('selected') else ' '} {m.get('weight')} - {m.get('mod').get('javapackage')} - {m.get('mod').get('name')}"


def getModule(query):
    matches = []

    for id, mod in cache.items():
        if id == query or mod.get('javapackage') == query:
            matches = [{"id": id, "mod": mod, "weight": 100}]
            break
        if re.search(r".*" + re.escape(query) + ".*", mod.get('javapackage')):
            weight = 25 if trl.match(mod.get(
                'javapackage')) else 75
            matches.append({"id": id, "mod": mod, "weight": weight})
        elif re.search(r".*" + re.escape(query.lower()) + ".*", mod.get('name').lower()):
            weight = 20 if trl.match(mod.get(
                'javapackage')) else 50
            matches.append({"id": id, "mod": mod, "weight": weight})

    if len(matches) == 0:
        print(f"Not found module for {query}")
        return None, None

    modId = None
    if (len(matches) == 1):
        modId = matches[0].get('id')
        print(f"Unique match for {query}:\n{matchToString(matches[0])}")
    else:
        matches.sort(key=lambda k: k['weight'], reverse=True)
        if matches[0].get('weight') > matches[1].get('weight'):
            matches[0]["selected"] = True
            modId = matches[0].get('id')
            selected = True
        else:
            selected = False

        print(
            f"Found {len(matches)} potential matches {'(selected the most relevant)' if selected else '(be more specific)'} for {query}:")
        for m in matches:
            print(matchToString(m))

    if not modId:
        print(f"Not found a match for: {query}")
        return None, None
    module = cache.get(modId)
    return module, modId


def clone(dependencies):
    i = 0
    for dep in dependencies:
        i += 1
        if dep in ob3:
            continue
        mod = cache.get(dep)
        if not mod:
            print("not found ", dep)
            continue
        print(f"\n {i}/{len(dependencies)} - {mod['javapackage']}")
        if os.path.exists(mod['javapackage']):
            print("   Not clonning, directory already exists")
            continue
        subprocess.run(["git", "clone", mod.get('path')])


parser = argparse.ArgumentParser()
parser.add_argument('-c', '--clone',
                    dest='clone',
                    action='store_true',
                    help='perform git clone')
parser.add_argument('-v', '--verbose',
                    dest='verbose',
                    action='store_true',
                    help='perform git clone')
parser.add_argument('query', nargs=argparse.REMAINDER)
args = parser.parse_args()


def main():
    dependencies = []
    for q in args.query:
        module, moduleId = getModule(q)
        if module:
            print('\n')
            printDependencies(moduleId, dependencies)

    if len(messages.get('error')) > 0:
        print("\nErrors:")
        for e in messages.get('error'):
            print("  ", e)

    if len(messages.get('info')) > 0:
        print("Info:")
        for e in messages.get('info'):
            print("  ", e)

    if args.clone:
        clone(dependencies)
    elif len(dependencies) > 0:
        print("\nNot cloning. Execute with --clone option to perform actual clone")


if __name__ == '__main__':
    main()
