with data(c_bpartner_id, rows) as(
  values (
    'A6750F0D15334FB890C254369AC750A8', -- bp to copy
    5                                   -- number of copies to generate
   )
)
,ins_bp as (
  insert into c_bpartner
      (c_bpartner_id   , ad_client_id    , ad_org_id       ,
       createdby       , updatedby       , value           ,
       name            , c_bp_group_id   )
   select get_uuid()   ,ad_client_id     ,ad_org_id       ,
      createdby       , updatedby       , 'bp-' || s       ,
      'bp-' || s             , c_bp_group_id   
    from c_bpartner bp, data d, generate_series(1,d.rows) s
   where bp.c_bpartner_id = d.c_bpartner_id
  returning c_bpartner_id, name
)
select *
  from ins_bp;