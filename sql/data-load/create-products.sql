with data(m_product_id, rows) as(
  values (
    'DA7FC1BB3BA44EC48EC1AB9C74168CED', -- product to copy
    5                                   -- number of copies to generate
   )
)
,ins_prod as (
  insert into m_product
      (m_product_id         , ad_client_id         , ad_org_id            ,
       createdby            , updatedby            , value                ,
       name                 , c_uom_id             , m_product_category_id,
       c_taxcategory_id     )
   select get_uuid()        , ad_client_id         , ad_org_id            ,
       createdby            , updatedby            , 'pr-' || lpad(s::text, greatest(length(d.rows::text), 3), '0'),
       'pr-' || lpad(s::text, greatest(length(d.rows::text), 3), '0') , c_uom_id             , m_product_category_id,
       c_taxcategory_id     
    from m_product m, data d, generate_series(1,d.rows) s
   where m.m_product_id = d.m_product_id
   on conflict do nothing
  returning m_product_id, name
)
select *
  from ins_prod;

